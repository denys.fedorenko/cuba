(function () {
    let body = document.querySelector('body')
    let wrapper = document.createElement("div");
    wrapper.attachShadow({ mode: "open" });
    let div = document.createElement("div");

    div.innerHTML = `<div draggable="true" class="fedorenko_dragble" style='position: fixed; right: 0; top:0; width: 300px; height: 120px; background-color: white;border-radius:10px ; padding: 10px;border: 2px solid darkblue; z-index: 99999;'>
<input type='text' class='input' style='margin-bottom:20px;margin-right:10px; positionreletive; border-radius:10px; outline:none;'>
<button class='search_button' style="border-radius:10px; outline:none;" >Search</button>
<span style=" font-size:25px; cursor:pointer;position:absolute; top:5px; right:10px" class="close-window">X</span>
<p class="info" style="margin:0 0 20px 0;">Search element by selectors</p>
<div style='position: relative; display: flex;' class='buttons-field'>
<button class='prev_element btn ' style="margin-right:5px; border-radius:10px; outline:none;"> Prev. elem.</button>
<button class='next_elem btn ' style="margin-right:5px; border-radius:10px; outline:none;" >Next elem.</button>
<button class='parent_element btn ' style="margin-right:5px; border-radius:10px; outline:none;">Parent</button>
<button class='child_element btn ' action="" style="margin-right:5px; border-radius:10px; outline:none;">Child</button>
</div>
</div>`

    wrapper.appendChild(div);
    div.setAttribute("class", "app")

    wrapper.shadowRoot.appendChild(div)
    body.prepend(wrapper);

    let nextElement = div.querySelector(".next_elem"),
        prevElement = div.querySelector(".prev_element"),
        parentelement = div.querySelector(".parent_element"),
        childElement = div.querySelector(".child_element"),
        searchButton = div.querySelector(".search_button"),
        close = div.querySelector(".close-window"),
        input = div.querySelector(".input"),
        drag = div.querySelector(".fedorenko_dragble"),
        info = div.querySelector(".info"),
        buttonsField = div.querySelector(".buttons-field"),
        foundedElement

    searchButton.addEventListener("click", findElement)

    buttonsField.addEventListener("click", (e) => {
        if (e.target.classList.contains("btn") && foundedElement != null) {
            let action = e.target.innerText;
            makeSomeMove(action)
        }
    })

    function makeSomeMove(action) {
        let newItem;
        switch (action) {
            case 'Prev. elem.': newItem = foundedElement.previousElementSibling;
                break
            case 'Next elem.': newItem = foundedElement.nextElementSibling;
                break
            case 'Parent': newItem = foundedElement.parentElement;
                break
            case 'Child': newItem = foundedElement.firstElementChild;
                break
            default: newItem = null;
        }

        if (newItem) {
            foundedElement.style.border = "none";
            foundedElement = newItem
            foundedElement.scrollIntoView({ block: "center" })
            addOutline(foundedElement);
        }
        isPossibleTransition()
    }

    close.addEventListener("click", () => {
        drag.remove()
    })

    function addOutline(foundedElement) {
        foundedElement.style = "border:5px solid green";
    }

    function findElement() {
        if (foundedElement) foundedElement.style = "none"
        if (!input.value || !(foundedElement = document.querySelector(`${input.value}`))) {
            alert("I cant find this element")
            input.value = ""
            return
        }

        foundedElement = document.querySelector(`${input.value}`);
        addOutline(foundedElement);
        foundedElement.scrollIntoView({ block: "center" })
        isPossibleTransition()
        return foundedElement;
    }

    function isPossibleTransition() {
        !foundedElement.nextElementSibling ? nextElement.setAttribute("disabled", "disabled") : nextElement.removeAttribute("disabled")
        !foundedElement.previousElementSibling ? prevElement.setAttribute("disabled", "disabled") : prevElement.removeAttribute("disabled")
        !foundedElement.parentElement ? parentelement.setAttribute("disabled", "disabled") : parentelement.removeAttribute("disabled")
        !foundedElement.firstElementChild ? childElement.setAttribute("disabled", "disabled") : childElement.removeAttribute("disabled")
    }

    function dragElement(drag) {
        let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
        drag.onmousedown = dragMouseDown;

        function dragMouseDown(e) {
            pos3 = e.clientX;
            pos4 = e.clientY;
            wrapper.onmouseup = closeDragElement;
            wrapper.onmousemove = elementDrag;
        }

        function elementDrag(e) {
            pos1 = pos3 - e.clientX;
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;

            drag.style.top = (drag.offsetTop - pos2) + "px";
            drag.style.left = (drag.offsetLeft - pos1) + "px";
        }

        function closeDragElement() {
            wrapper.onmouseup = null;
            wrapper.onmousemove = null;
        }

        drag.ondragstart = function () {
            return false;
        };
    }

    dragElement(drag)
})()


/*javascript:!function(){let e=document.querySelector("body"),t=document.createElement("div");t.attachShadow({mode:"open"});let n=document.createElement("div");n.innerHTML='<div draggable="true" class="fedorenko_dragble" style=\'position: fixed; right: 0; top:0; width: 300px; height: 120px; background-color: white;border-radius:10px ; padding: 10px;border: 2px solid darkblue; z-index: 99999;\'>\n<input type=\'text\' class=\'input\' style=\'margin-bottom:20px;margin-right:10px; positionreletive; border-radius:10px; outline:none;\'>\n<button class=\'search_button\' style="border-radius:10px; outline:none;" >Search</button>\n<span style=" font-size:25px; cursor:pointer;position:absolute; top:5px; right:10px" class="close-window">X</span>\n<p class="info" style="margin:0 0 20px 0;">Search element by selectors</p>\n<div style=\'position: relative; display: flex;\' class=\'buttons-field\'>\n<button class=\'prev_element btn \' style="margin-right:5px; border-radius:10px; outline:none;"> Prev. elem.</button>\n<button class=\'next_elem btn \' style="margin-right:5px; border-radius:10px; outline:none;" >Next elem.</button>\n<button class=\'parent_element btn \' style="margin-right:5px; border-radius:10px; outline:none;">Parent</button>\n<button class=\'child_element btn \' action="" style="margin-right:5px; border-radius:10px; outline:none;">Child</button>\n</div>\n</div>',t.appendChild(n),n.setAttribute("class","app"),t.shadowRoot.appendChild(n),e.prepend(t);let l,r=n.querySelector(".next_elem"),i=n.querySelector(".prev_element"),o=n.querySelector(".parent_element"),s=n.querySelector(".child_element"),d=n.querySelector(".search_button"),a=n.querySelector(".close-window"),u=n.querySelector(".input"),c=n.querySelector(".fedorenko_dragble"),b=(n.querySelector(".info"),n.querySelector(".buttons-field"));function p(e){e.style="border:5px solid green"}function m(){l.nextElementSibling?r.removeAttribute("disabled"):r.setAttribute("disabled","disabled"),l.previousElementSibling?i.removeAttribute("disabled"):i.setAttribute("disabled","disabled"),l.parentElement?o.removeAttribute("disabled"):o.setAttribute("disabled","disabled"),l.firstElementChild?s.removeAttribute("disabled"):s.setAttribute("disabled","disabled")}d.addEventListener("click",function(){l&&(l.style="none");if(!u.value||!(l=document.querySelector(`${u.value}`)))return alert("I cant find this element"),void(u.value="");return p(l=document.querySelector(`${u.value}`)),l.scrollIntoView({block:"center"}),m(),l}),b.addEventListener("click",e=>{if(e.target.classList.contains("btn")&&null!=l){!function(e){let t;switch(e){case"Prev. elem.":t=l.previousElementSibling;break;case"Next elem.":t=l.nextElementSibling;break;case"Parent":t=l.parentElement;break;case"Child":t=l.firstElementChild;break;default:t=null}t&&(l.style.border="none",(l=t).scrollIntoView({block:"center"}),p(l));m()}(e.target.innerText)}}),a.addEventListener("click",()=>{c.remove()}),function(e){let n=0,l=0,r=0,i=0;function o(t){n=r-t.clientX,l=i-t.clientY,r=t.clientX,i=t.clientY,e.style.top=e.offsetTop-l+"px",e.style.left=e.offsetLeft-n+"px"}function s(){t.onmouseup=null,t.onmousemove=null}e.onmousedown=function(e){r=e.clientX,i=e.clientY,t.onmouseup=s,t.onmousemove=o},e.ondragstart=function(){return!1}}(c)}();*/